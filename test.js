const DriverBuilder = require('./DriverBuilder');
const Action = require('./Action');
const assert = require('chai').assert;
let driver = null;
let action = null;

describe('Calculator flow', async function testingFlow() {
    this.timeout(60000);
    before(async function beforeTest() {
        driver = await DriverBuilder.setUpDriver('chrome');
        action = new Action(driver);
    });
    it('Check full summ', async () => {
        await action.clickButton('option__flat', 'id');
        await action.moveSlider('#noUiRange_4WsjpwBh .noUi-handle-lower', 5, 7);
        await action.clickButton('concept__business', 'id');
        await action.clickButton('u-btn-submit', 'class');
        await action.clickButton('additional-service__montazhnye-raboty', 'id');
        await action.clickButton('u-checkbox__functions', 'class');
        await action.clickButton('u-btn-functions-learn-more', 'class');
        await action.clickButton('subfunction_izmenenie-yarkosti-dimmirovanie', 'id');
        await action.clickButton('product_check_klavishnye-sensory-jung-serii-f50', 'id');
        await action.clickButton('product_check_klavishnye-sensory-jung-serii-f40', 'id');
        await action.clickButton('product_check_klavishnye-sensory-berker234', 'id');
        await driver.sleep(3000);
        await action.clickButton('save-function', 'id');
        //Get final price
        let finalPrice = await driver.executeScript(
            'return document.querySelector(\'.total-summ\').innerText;'
        );
        assert.isNotNaN(parseInt(finalPrice));
        assert(parseInt(finalPrice) > 0, 'Total price is zero');
    });
    it('Change base params', async () => {

        await action.clickButton('change-house-params', 'id');
        await action.moveSlider('#noUiRange_4WsjpwBh .noUi-handle-lower', 10, 7);
        const newCountProduct = await driver.executeScript(
            'return document.querySelector(\'#noUiRange_4WsjpwBh .noUi-handle-lower\').getAttribute(\'aria-valuetext\');'
        );
        await action.clickButton('u-btn-submit', 'class');
        await action.clickButton('u-btn-functions-learn-more', 'class');
        await action.clickButton('product_check_klavishnye-sensory-jung-serii-f50', 'id');
        const countProductAfter = await driver.executeScript(
            'return document.querySelector(\'#subfunction_izmenenie-yarkosti-dimmirovanie .noUi-handle-lower\').getAttribute(\'aria-valuetext\');'
        );
        // await action.clickButton('save-function', 'id');
        assert.equal(newCountProduct, countProductAfter, 'Changing options does not work');
    });
    it('Send form', async () => {
        await action.clickButton('send-mail', 'id');
        await action.fillInput('user-name', 'test');
        await action.fillInput('user-email', 'test@gmail.com');
        await action.fillInput('user-phone', '12345677654321');
        const result = await action.sendForm('send-form');
        assert.equal(result, 'Сообщение успешно отправлено');
    });
    after(async () => {
        // await DriverBuilder.quit();
    });
});
