const webdriver = require('selenium-webdriver');
require('dotenv').config();

class DriverBuilder {
  async setUpDriver(browser) {
    this.browser = browser;
    switch (this.browser) {
      case 'chrome':
        const chromeCapabilities = webdriver.Capabilities.chrome();
        //setting chrome options to start the browser fully maximized
        const chromeOptions = {
          'args': [
              '--test-type',
              '--start-maximized',
              '--auto-open-devtools-for-tabs',
              '--load-extension=vue-devtools'
          ]
        };
        chromeCapabilities.set('chromeOptions', chromeOptions);
        this.driver = await new webdriver.Builder()
          .forBrowser('chrome')
          .withCapabilities(chromeCapabilities)
          .build();
        break;
      case 'firefox':
        this.driver = await new webdriver.Builder()
          .forBrowser('firefox')
          .build();
        break;
      default:
        break;
    }
    this.driver.get(
      process.env.SITE_URL,
    );
    return this.driver;
  }
  async quit() {
    await this.driver.quit();
  }
}

module.exports = new DriverBuilder();
