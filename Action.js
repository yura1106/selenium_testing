const webdriver = require('selenium-webdriver'),
      By = webdriver.By;
class Action {
    constructor(driver) {
        this.driver = driver;
    }
    async clickButton(button, type) {
        // await this.sleep(3000);
        if(type === 'class')
            this.button = await this.findElementByClassName(button);
        else if(type === 'id')
            this.button = await this.findElementById(button);
        await this.button.click();
        return true;
    }

    // find one element by class name
    async findElementByClassName(selector) {
        return this.driver.wait(
            webdriver.until.elementLocated(webdriver.By.className(selector)),
            10000,
        );
    }
    // find by id
    async findElementById(selector) {
        return this.driver.wait(
            webdriver.until.elementLocated(webdriver.By.id(selector)),
            5000,
            'Element is not found'
        );
    }
    // moveSlider
    async moveSlider(selector, amountElementsMove, elementWidth) {
        const actions = this.driver.actions(),
            byElement = By.css(selector),
            e3 = this.driver.findElement(byElement);
        await this.driver.executeScript(
            'return document.querySelector(\''+ selector + '\').setAttribute(\'aria-valuetext\', 5);'
        );
        const elCoord = await this.driver.executeScript(
            'return document.querySelector(\''+ selector + '\').getBoundingClientRect();'
        );
        const location = {x: amountElementsMove*elementWidth, y: parseInt(elCoord.y)};
        await actions.dragAndDrop(e3, location).perform();
        return true;
    }

    async fillInput(selector, info) {
        this.element = await this.findElementById(selector);
        this.element.clear();
        this.element.sendKeys(info);
    }

    async sendForm(selector) {
        this.element = await this.findElementById(selector);
        // await this.driver.sleep(3000);
        this.element.sendKeys(webdriver.Key.ENTER);
        const successElement = await this.findElementByClassName('result-message');
        // console.log(successElement);
        await this.isVisible(successElement);

        return this.driver.executeScript(
            'return document.querySelector(\'.result-message\').textContent;'
        );
    }

    async isVisible(element) {
        return this.driver.wait(webdriver.until.elementIsVisible(element), 10000);
    }
}

module.exports = Action;